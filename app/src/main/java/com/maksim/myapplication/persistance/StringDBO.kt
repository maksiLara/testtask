package com.maksim.myapplication.persistance

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*


@Entity(tableName = "string_value")
data class StringDBO(
    @PrimaryKey
    @ColumnInfo(name = "value") val value: String?
)