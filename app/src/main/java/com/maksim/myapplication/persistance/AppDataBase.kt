package com.maksim.myapplication.persistance

import androidx.room.Database
import androidx.room.RoomDatabase


@Database(entities = [StringDBO::class], version = 1)
internal abstract class AppDatabase : RoomDatabase() {

    abstract fun stringDao(): StringDao
}


