package com.maksim.myapplication.persistance

import android.app.Application
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.MainThread
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.maksim.myapplication.R
import kotlinx.android.synthetic.main.fragment_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*


class MainFragment : Fragment(R.layout.fragment_main) {

    var quantity: Long = 0

    private var listener: View.OnClickListener? = null
    private val viewModel: StringViewModel by viewModel()

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as? View.OnClickListener
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val numberAdapter = NumberAdapter()
        numbersRecycler.layoutManager = LinearLayoutManager(activity)


        viewModel?.items?.observe(this, androidx.lifecycle.Observer { items ->
            items?.let {
                numberAdapter?.addValue(it)
            }
        })
        btnFragmentAdd?.setOnClickListener {
            val item = StringDBO("$quantity")
            viewModel.add(item)
            quantity++
        }
        btnFragmentDelete?.setOnClickListener {
            val item = StringDBO("$quantity")
            viewModel.remove(item)
            quantity--
        }
    }
}