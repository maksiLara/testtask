package com.maksim.myapplication.persistance

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch

class StringViewModel(private val repository: MainRepository) : ViewModel() {

    var items: LiveData<List<StringDBO>> = repository.allItems

    fun add(stringDBO: StringDBO) = viewModelScope.launch { repository.add(stringDBO) }
    fun remove(stringDBO: StringDBO) = viewModelScope.launch { repository.remove(stringDBO) }
}