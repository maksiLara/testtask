package com.maksim.myapplication.persistance

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface StringDao {
    @Query("SELECT * FROM string_value")
    fun getAll(): LiveData<List<StringDBO>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun add(item: StringDBO)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun addItems(item: List<StringDBO>)

    @Delete
    fun remove(item: StringDBO)

    @Query("DELETE FROM string_value")
    fun removeAll()


}