package com.maksim.myapplication.persistance


import androidx.lifecycle.LiveData

class MainRepository(private val stringDao: StringDao) {

    val allItems: LiveData<List<StringDBO>> = stringDao.getAll()

    fun add(stringDBO: StringDBO) {
        stringDao.add(stringDBO)
    }
    fun remove(stringDBO: StringDBO) {
        stringDao.remove(stringDBO)
    }

}